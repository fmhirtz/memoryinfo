
- **MemTotal:** Total usable RAM (i.e. physical RAM minus a few reserved bits and the kernel binary code)
===
- **MemFree:** The amount of physical memory not used by the system
- MemAvailable: An estimate of the amount of memory available for userspace allocation *without causing swapping*
   - The idea behind this field is that the kernel devs can define and adjust a function to provide a user consumable esitmate of how much memory that they have to work with.

   - The current function follows to give you an idea of how this is calculated:

~~~
    /* Estimate the amount of memory available for userspace allocations,
    * without causing swapping.
    */
   available = global_zone_page_state(NR_FREE_PAGES) - totalreserve_pages;

   /*
    * Not all the page cache can be freed, otherwise the system will
    * start swapping. Assume at least half of the page cache, or the
    * low watermark worth of cache, needs to stay.
    */
   pagecache = pages[LRU_ACTIVE_FILE] + pages[LRU_INACTIVE_FILE];
   pagecache -= min(pagecache / 2, wmark_low);
   available += pagecache;

   /*
    * Part of the reclaimable slab and other kernel memory consists of
    * items that are in use, and cannot be freed. Cap this estimate at the
    * low watermark.
    */
   reclaimable = global_node_page_state(NR_SLAB_RECLAIMABLE) +
         global_node_page_state(NR_KERNEL_MISC_RECLAIMABLE);
   available += reclaimable - min(reclaimable / 2, wmark_low);
   ~~~

- **Cached:** In-memory cache for files read from the disk (the pagecache). Does not include SwapCached
   - This is comprised of: 
      - nr_file_pages(not presented in meminfo) - Buffers - SwapCached   
- **Buffers:** Relatively temporary storage for raw disk blocks shouldn’t get tremendously large (20MB or so)
- **SwapCached:** Memory that once was swapped out, is swapped back in but still also is in the swapfile (if memory is needed it doesn’t need to be swapped out AGAIN because it is already in the swapfile. This saves I/O)
- **Active:** Memory that has been used more recently and usually not reclaimed unless absolutely necessary.
   - This is comprised of:
      - "Active(anon)"+"Active(file)"
         - **Active(anon):** Anonymous memory that has been used more recently and usually not swapped out
         - **Active(file):** Pagecache memory that has been used more recently and usually not reclaimed until needed 
- **Inactive:** Memory which has been less recently used. It is more eligible to be reclaimed for other purposes
   - This is comprised of 
      - "Inactive(anon)"+"Inactive(file)"
         - **Inactive(anon):** Anonymous memory that has not been used recently and can be swapped out
         - **Inactive(file):** Pagecache memory that can be reclaimed without notable performance impact
- **Slab:** In-kernel data structures cache
   - This is comprised of:
      - **SReclaimable:** Slab that might be reclaimed, such as caches
      - **SUnreclaim:** Slab that cannot be reclaimed on memory pressure
- **Unevictable:** The amount of memory, discovered by the pageout code, that is not evictable because it is locked into memory by user programs.
- **Mlocked:**  The total amount of memory, that is not evictable because it is locked into memory by user programs.

- **SwapTotal:** The total amount of swap available
- **SwapFree:** The amount of swap that is free

- **Dirty:** Memory which is waiting to get written back to the disk
- **Writeback:** Memory which is actively being written back to the disk
- **AnonPages:** Non-file backed pages mapped into userspace page tables
- **Mapped:** Files which have been mmaped, such as libraries
- **Shmem:** Total memory used by shared memory (shmem) and tmpfs
- **KReclaimable:** Kernel allocations that the kernel will attempt to reclaim under memory pressure. Includes SReclaimable (below), and other direct allocations with a shrinker.
- **KernelStack:** The amount of memory used by the kernel stack allocations done for each task in the system.
- **PageTables:** The amount of memory dedicated to the lowest level of page tables. 
- **NFS_Unstable:** Always zero. Previous counted pages which had been written to the server, but has not been committed to stable storage.
- **Bounce:** The amount of memory used for the block device "bounce buffers".
WritebackTmp:** The amount of memory used by FUSE for temporary writeback buffers.

- **CommitLimit:** For "memory overcommit", the current calculated limt based on the set overcommit_ratio
- **Committed_AS:** For "memory overcommit", the amount of memory on the host that's been (potentially) allocated

- **VmallocTotal:** The total amount of memory of total allocated virtual address space.
- **VmallocUsed:** The total amount of memory of used virtual address space.
- **VmallocChunk:** The largest contiguous block of memory of available virtual address space.

- **Percpu:** Memory allocated to the percpu allocator used to back percpu allocations. This stat excludes the cost of metadata.
- **HardwareCorrupted:** The amount of RAM the kernel identified as corrupted / not working

- **AnonHugePages:** The ammount of memory backed by transparent hugepages
- **ShmemHugePages:** The amount of shared (shmem/tmpfs) memory backed by hugepages.
- **ShmemPmdMapped:** The amount of shared memory mapped into userspace with hugepages

- **HugePages_Total:** Number of hugepages being allocated by the kernel (Defined with vm.nr_hugepages (explicitly allocated hugepages))
- **HugePages_Free:** The number of hugepages not in use.
- **HugePages_Rsvd:** The number of hugepages for which a commitment to allocate from the pool has been made, but no allocation has yet been made.
- **HugePages_Surp:** The number of hugepages in the pool above the value in vm.nr_hugepages. The maximum number of surplus hugepages is controlled by vm.nr_overcommit_hugepages.
- **Hugepagesize:** The default hugepage size
- **Hugetlb:**  the total amount of memory (in kB), consumed by huge pages of all sizes. If huge pages of different sizes are in use, this number will exceed HugePages_Total * Hugepagesize. To get more detailed information, please, refer to /sys/kernel/mm/hugepages
- **DirectMap4k:** The amount of memory being mapped to standard 4k pages
- **DirectMap2M:** The amount of memory being mapped to hugepages (usually 2MB in size)

